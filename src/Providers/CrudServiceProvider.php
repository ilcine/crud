<?php
namespace Emr\Crud;  // src directory

use Illuminate\Support\ServiceProvider;
use Emr\Crud\Http\Controllers\TaskController;


class CrudServiceProvider extends ServiceProvider
{

	public function boot()
	{
		 // Automatic loading
		 $this->loadRoutesFrom( __DIR__ .'/../../routes/web.php');
		 $this->loadViewsFrom( __DIR__ .'/../../resources/views', 'couriercrud'); // Use `couriercrud::` in the Controllers section
		 $this->loadMigrationsFrom( __DIR__ .'/../../database/migrations');
		 
	}
	
	public function register()
	{
		
	}
    
}
