<?php
//namespace App\Http\Controllers;  //  *** DEFAULT CONTROLLER ***
namespace Emr\Crud\Http\Controllers;  // for pacagist

use App\Http\Controllers\Controller;

//use App\Task;  //  *** DEFAULT MODEL  ****
use Emr\Crud\Task;  // Model name

use Illuminate\Support\Facades\Input; // Input kontrol 
use Illuminate\Http\Request; // Request kontrol

//CRUD (Yaratma,Okuma,Güncelleme, Silme) functionları bu sınıf içine yazalım 
class TaskController extends Controller
{
  //  Listeleme  
  public function TaskListesi()  // [get] Methodu
  {
		 
  $tasks = Task::all();
		 
  return view('couriercrud::TaskListesi',compact('tasks'));  // ifade ['tasks' => $tasks] şeklinde deolabilir.
  }

  public function TaskAra()  // [post] Medhodu
  {
  $ArananKayit = Input::get('txtAra');
  $ButonAra = Input::get('btn');
   
  if ($ButonAra == "ara") 
    {     
    $tasks = Task::where('name', 'LIKE', "%$ArananKayit%" )->get();
    }
    else
    {
    $msg = 'Don\'t found';  
    }  
     
  return view('couriercrud::TaskListesi', compact('tasks'));  
  }

  //Güncelleme
  public function TaskDetay($_id)  // [get] Methodu
  {
    $tasks = Task::find(decrypt($_id));
		 
    return view('couriercrud::TaskDetay', compact('tasks'));
  }

  public function TaskDetayGuncelle($_id, Request $request) //[post]
  {
    $this->validate($request, [ 'name' => 'required|max:50' ]);  // Uzunluk kontrol        
    $tasks = Task::find(decrypt($_id));
    $tasks->name  = $request->get('name');
    $tasks->save();
    $msg = 'Record update ok.';
    return redirect('/crud/TaskListesi')->with('status',$msg);
  }

  //Yeni Kayıt oluşturma
  public function TaskYeniKayit() // [get] Methodu
  {
    $tasks     = new Task;
    $tasks->id   = null;
    $tasks->name  = "";
    $tasks->created_at = 0;
    $tasks->updated_at = 0;
    $msg       = 'New record ok.'; // geri dönecek mesaj
    return view('couriercrud::TaskYeniKayit', compact('tasks'));
  }

  public function TaskYeniKayitYaz($_id, Request $request) //[post] Methodu
  {    
    //dd(decrypt($_id));  // Test gelen kayda bak
    //dd($request->get('name'));  // Test gelen kayda bak.
    $this->validate($request, ['name' => 'required|max:50' ]); // uzunluk kontrolu
    $tasks         = new Task;  // modeli al
    $tasks->id     = null;
    $tasks->name  = $request->get('name');
    $tasks->created_at   ='2017-01-01 00:00:00';
    $tasks->updated_at  ='2017-01-01 00:00:00';
    $tasks->save();
    $msg       = 'New record write ok.'; // geri dönecek mesaj
    return redirect('/crud/TaskListesi')->with('status',$msg);
  }  

  //Silme işlemi
  public function TaskSilinecek($_id) //[get] Methodu
  {    
    $tasks = Task::find(decrypt($_id));    
    return view('couriercrud::TaskSilinecek', compact('tasks'));
  }

  public function TaskSil($_id) //[post] Methodu
  {    
    //dd(decrypt($_id));  // Test id ye bak.
    $tasks = Task::find(decrypt($_id));    
    $tasks->delete();
    $msg = 'Record deleted ok.'; // geri dönecek mesaj.
    return redirect('/crud/TaskListesi')->with('status',$msg);
  }

} //  class TaskController sonu
