<?php
//namespace App;  // default
namespace Emr\Crud;

use Illuminate\Database\Eloquent\Model;
class Task extends Model
{
        protected $table = 'task';
        protected $primaryKey = 'id';
        public  $incrementing = true;
        protected $fillable = [ 'name'];
}
