<?php

  // Main Group
	Route::group([
		'namespace' => "Emr\Crud\Http\Controllers",
		'prefix' => 'crud',
		'middleware' => ['web'], ],  function () {

		// First page; http://{domain}/crud/TaskListesi
		 Route::get('/TaskListesi',    'TaskController@TaskListesi');
		Route::post('/TaskListesi',    'TaskController@TaskAra');

		 Route::get('/TaskDetay/{id}',    'TaskController@TaskDetay');  
		Route::post('/TaskDetay/{id}',    'TaskController@TaskDetayGuncelle');

		 Route::get('/TaskYeniKayit/{id}',    'TaskController@TaskYeniKayit');
		Route::post('/TaskYeniKayit/{id}',    'TaskController@TaskYeniKayitYaz');  

		 Route::get('/TaskSil/{id}',    'TaskController@TaskSilinecek');
		Route::post('/TaskSil/{id}',    'TaskController@TaskSil');

});


 
