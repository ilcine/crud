@extends('couriercrud::layouts.appBasit')
@section('content')

<h2> Remove </h2>

@if (session('status'))
	<p>{{ session('status') }}</p>
@endif

<form method="post" action="{!! action('\Emr\Crud\Http\Controllers\TaskController@TaskSil', encrypt($tasks->id)) !!}">
	<input type="hidden" name="_token" value="{!! csrf_token() !!}">

		<table style="width:50%">
			<tbody>
				<tr>
					<td> <p><b>{!! $tasks->name !!} </b> </p></td> 		
					<td> <a href="{!! action('\Emr\Crud\Http\Controllers\TaskController@TaskListesi', null) !!}" class="ghost-button" > Cancel </a></td> 
					<td> <button type="submit" class="ghost-button"> Remove record </button> </td>
				</tr>
			</tbody>
		</table>	 	
		
</form>
		 
@endsection


