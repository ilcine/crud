<!DOCTYPE html>
<html lang="tr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

<style>
nav {
  background-color: #333;
  margin: 0;
  overflow: hidden;
}
nav ul{ 
  margin: 0;
  padding: 0;
}
nav ul li {
  display: inline-block;
  list-style-type: none;
  color: #000
}
nav > ul > li > a { 
  color: #ff0;
  display: block;
  line-height: 2em;
  padding: 0.5em 0.5em;
  text-decoration: none;
}

.ghost-button {
 color: #009688;
 background: #fff;
 border: 1px solid #009688;
 font-size: 17px;
 padding: 7px 12px;
 font-weight: normal;
 margin: 6px 0;
 margin-right: 12px;
 display: inline-block;
 text-decoration: none;
 font-family: 'Open Sans', sans-serif;
 min-width: 80px;
}
.ghost-button:hover, .ghost-button:active {
 color:#fff;
 background:#009688;
}

input {
  padding: 7px 12px;
  margin: 6px 0;
  margin-right: 12px;
}

table {
    border-collapse: collapse;
    width: 100%;
}
th {
    background-color: #DDEFEF;
    border: solid 1px #DDEEEE;
    color: #336B6B;
    padding: 10px;
    text-align: left;
}
td {
    padding: 8px;
    text-align: left;
    border-bottom: 1px solid #ddd;
}
tr:hover {background-color:#f5f5f5;}

</style>
</head>

<body>
  <div>
    <nav>
        <ul>
          <li> <a href="/crud/TaskListesi"> <b>CRUD</b> </a> </li>
          <li> <a href="/crud/TaskListesi"> <b style="color:orange">   ||| List ||| </b> </a> </li>
          <li> <a href="/crud/TaskYeniKayit/0"> <b style="color:greenyellow"> ||| Create new record |||</b> </a> </li>
        </ul>
    </nav>
    
    <div align="center">
    @yield('content')    
    </div>
  </div>
</body>

</html>
