@extends('couriercrud::layouts.appBasit')
@section('content')

<h2> New record </h2>
 
@foreach ($errors->all() as $error) 
	<p>{{ $error }}</p> 
@endforeach

@if (session('status')) 
	<p> {{ session('status') }}  </p> 
@endif
			
<form method="post"  enctype="multipart/form-data" >		
	<input type="hidden" name="_token" value="{!! csrf_token() !!}">
		
	<table style="width:40%">	
		<tbody>
			<tr>
				<td> <input type="text" name="name" value="{!! $tasks->name !!}"> </td> 		
				<td> <a href="{!! action('\Emr\Crud\Http\Controllers\TaskController@TaskListesi', null) !!}" class="ghost-button"> Cancel </a> </td> 
				<td> <button type="submit" class="ghost-button"> New record</button> </td>
			</tr>
		</tbody>	
	</table>
	
</form>
 
@endsection


