@extends('couriercrud::layouts.appBasit')
@section('content')

<h2> Detail </h2>
		
@foreach ($errors->all() as $error) 
	<p>{{ $error }}</p> 
@endforeach

@if (session('status')) 
	<p> {{ session('status') }}  </p> 
@endif
		
<form method="post" enctype="multipart/form-data" >
	<input type="hidden" name="_token" value="{!! csrf_token() !!}">
	<table style="width:50%">
		<tbody>
			<tr>
				<td> <input type="text" name="name" value="{!! $tasks->name !!}"></td> 		
				<td> 
					<a href="{!! action('\Emr\Crud\Http\Controllers\TaskController@TaskListesi',
					null) !!}" class="ghost-button" > Cancel </a>
				</td> 
				<td> <button type="submit" class="ghost-button"> Update </button> </td>
				<td> 
					<a href ="{!! action('\Emr\Crud\Http\Controllers\TaskController@TaskSilinecek', 
					encrypt($tasks->id)) !!}" class="ghost-button" > Remove </a> 
				<td>	
			</tr>
		</tbody>
	</table>	 
</form>
	
@endsection
