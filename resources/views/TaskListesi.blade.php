@extends('couriercrud::layouts.appBasit')
@section('content')
<h2> List </h2>
<div>
	@if ($tasks->isEmpty()) <p> No record</p> 
		@else
		  @if (session('status'))	
		  <p> {{ session('status') }} </p>
		  @endif
</div>
	
	<!-- Arama Bölümü -->
	<table style="width:40%">
	  <tr>			
			<td>
				<form method="post">
					<input type="hidden" name="_token" value="{!! csrf_token() !!}">
					<input type="text" name="txtAra" value="">
			</td>
			<td>			
					<button name="btn" value="ara" class="ghost-button" >Search</button>	 
				</form>
			</td>
		</tr>
	</table>	
	<br>	
	
	<!-- listeleme Bölümü -->	
	<table style="width:40%">
		<thead>
			<tr>
				<th style="width:10%">&nbsp;ID&nbsp;</th>
				<th style="width:30%">&nbsp;Name&nbsp;</th> 
				<th>&nbsp;Detail</th>	
				<th>&nbsp;Remove&nbsp;</th>
			</tr>
		</thead>
		
		<tbody>
			
			
			@foreach($tasks as $key => $task)
			<tr>		
				<td> {{ $task->id }} </td>
				<td> {!! $task->name !!} </td>					
        <td> 
					<a href="{!! action('\Emr\Crud\Http\Controllers\TaskController@TaskDetay',
				       encrypt($task->id)) !!}"  class="ghost-button"> Detail </a> 
				</td>	
        <td> 
					<a href="{!! action('\Emr\Crud\Http\Controllers\TaskController@TaskSilinecek', 
							encrypt($task->id)) !!}" class="ghost-button"> Remove </a> 
				</td>
				
			</tr>	
			@endforeach
			
		</tbody>
	</table>
    
	@endif		       
@endsection


